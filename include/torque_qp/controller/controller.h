/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#pragma once
// Pinocchio
#include "pinocchio/fwd.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/aba.hpp"
#include "pinocchio/algorithm/rnea.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/multibody/model.hpp"

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

// Ros specific
#include <ros/param.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include <ros/time.h>

// Eigen
#include <Eigen/Dense>

// //Boost
#include <boost/scoped_ptr.hpp>

// qpOASES
#include <qp_solver/qp_solver.hpp>
#include <qpOASES.hpp>

// Conversion tool
#include <eigen_conversions/eigen_msg.h>

// Messages
#include <realtime_tools/realtime_publisher.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <torque_qp/PandaRunMsg.h>
#include <torque_qp/UI.h>

// TrajectoryGenerator
#include <panda_traj/panda_traj.hpp>



using namespace pinocchio;

namespace Controller
{
class Controller
{
public:
    /**
    * \fn bool init
    * \brief Initializes the controller
    * \param ros::NodeHandle& node_handle a ros node handle
    * \param Eigen::VectorXd q_init the robot initial joint position
    * \param Eigen::VectorXd qd_init the robot initial joint velocity
    * \return true if the controller is correctly initialized
    */
    bool init(ros::NodeHandle& node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qd_init);

    /**
     * \fn Eigen::VectorXd update
     * \brief Update the controller to get the new desired joint torque to send to the robot.
     * \param Eigen::VectorXd q the current joint position of the robot
     * \param Eigen::VectorXd qd the current joint velocity of the robot
     * \param const ros::Duration& period the refresh rate of the control
     * \return A Eigen::VectorXd with the desired joint torque
     */
    Eigen::VectorXd update(Eigen::VectorXd q, Eigen::VectorXd qd, const ros::Duration& period);

  template <class T>
  /**
    * \fn bool getRosParam
    * \brief Load a rosparam to a variable
    * \param const std::string& param_name the name of the ros param
    * \param T& param_data the variable linked to the ros param
    * \return true if the ros param exist and can be linked to the variable
    */
  bool getRosParam(const std::string& param_name, T& param_data, bool verbose = false)
  {
    if (!ros::param::get(param_name, param_data))
    {
      ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
      ros::shutdown();
    }
    else
    {
      if (verbose)
        ROS_INFO_STREAM(param_name << " : " << param_data);
    }
    return true;
  }

  /**
    * \fn bool getRosParam
    * \brief Load a rosparam to a variable
    * \param const std::string& param_name the name of the ros param
    * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
    * \return true if the ros param exist and can be linked to the variable
    */
  bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false)
  {
    std::vector<double> std_param;
    if (!ros::param::get(param_name, std_param))
    {
      ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
      ros::shutdown();
    }
    else
    {
      if (std_param.size() == param_data.size())
      {
        for (int i = 0; i < param_data.size(); ++i)
          param_data(i) = std_param[i];
        if (verbose)
          ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
        return true;
      }
      else
      {
        ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros");
        ros::shutdown();
      }
    }
  }
  
    /**
    * \fn void init_publishers
    * \brief Initializes publishers
    * \param ros::NodeHandle& node_handle a ros node handle
    */
    void initPublishers(ros::NodeHandle& node_handle);

    /**
    * \fn void load_parameters
    * \brief Loads parameters from yaml file
    */
    void loadParameters();

    /**
    * \fn bool load_robot 
    * \brief Loads Panda robot
    * \param ros::NodeHandle& node_handle a ros node handle
    * \return true if the robot is correctly loaded
    */
    bool loadRobot(ros::NodeHandle& node_handle);

    /**
    * \fn void do_publishing
    * \brief Publishes values
    * \return publish messages
    */
    void doPublishing();

    /**
    * \fn void buildTrajectory
    * \brief Build the trajectory
    * \param KDL::Frame X_curr the current pose of the robot
    */
    void buildTrajectory(Eigen::Affine3d X_curr);

    /**
    * @brief Publish the trajectory
    */
    void publishTrajectory();

    /**
    * @brief ros service to interact with the robot
    */
    bool updateUI(torque_qp::UI::Request& req, torque_qp::UI::Response& resp);
    
    /**
    * @brief ros service to update the trajectory
    */
    bool updateTrajectory(panda_traj::UpdateTrajectory::Request &req, panda_traj::UpdateTrajectory::Response &resp);
  
    int dof; /*!< @brief Number of degrees of freedom of the robot */
    std::string tip_link; /*!< @brief tip link of the KDL chain (usually the end effector*/
    bool sim = false;  /*!< @brief Get if code is on the real robot to remove the gravity. False by defaultB*/

     // Publishers
    geometry_msgs::Pose X_curr_msg; /*!< @brief geometry_msgs::Pose message containing the robot current pose */
    geometry_msgs::Pose X_traj_msg; /*!< @brief geometry_msgs::Pose message containing the robot desred pose given by the trajectory generation*/ 
    geometry_msgs::Twist X_err_msg; /*!< @brief geometry_msgs::Twist message containint the error between the robot desired and current pose*/
    realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> pose_array_publisher; /*!< @brief ROS publisher of an array of pose of the trajectory*/
    realtime_tools::RealtimePublisher<nav_msgs::Path> path_publisher; /*!< @brief ROS publisher of a message containing the path of the trajectory */
    realtime_tools::RealtimePublisher<torque_qp::PandaRunMsg> panda_rundata_publisher; /*!< @brief ROS publisher of a custom message to publish data related to torque_qp */
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_curr_publisher; /*!< @brief ROS publisher of the robot current position message */
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_des_publisher; /*!< @brief ROS publisher of the robot desired position message*/

    // Services
    ros::ServiceServer updateUI_service; /*!< @brief ROS service to update the user interface */
    ros::ServiceServer updateTraj_service; /*!< @brief ROS service to update the trajectory*/

    std::shared_ptr<pinocchio::Model> model; /*!< @brief pinocchio model*/ 
    std::shared_ptr<pinocchio::Data> data; /*!< @brief pinocchio data*/

    Eigen::VectorXd nonLinearTerms; /*!< @brief Matrix representing M^{-1} ( g(q) + c(q,qd) ) */
    Eigen::MatrixXd J; /*!< @brief Jacobian matrix */
    Eigen::MatrixXd Jdot; /*!< @brief Jacobian derivative */

    Eigen::VectorXd p_gains; /*!< @brief Proportional gains of the PID controller */ 
    Eigen::VectorXd i_gains; /*!< @brief Derivative gains of the PID controller */
    Eigen::VectorXd d_gains; /*!< @brief Integral gains of the PID controller */

    Eigen::VectorXd joint_torque_out; /*!< @brief Results of the QP optimization */

    Eigen::Matrix<double, 6, 1> xdd_des; /*!< @brief Desired robot acceleration of the robot tip_link */
    Eigen::Matrix<double, 6, 1> err; /*!< @brief Eigen desired Cartesian error between the desired and current pose */

    // Matrices for qpOASES
    // NOTE: We need RowMajor (see qpoases doc)
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> H; /*!< @brief Hessian matrix of the QP*/
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> A; /*!< @brief Constraint matrix of the QP */

    Eigen::VectorXd g; /*!< @brief Gradient vector of the QP */
    Eigen::VectorXd lb; /*!< @brief Lower bound vector of the QP */
    Eigen::VectorXd ub; /*!< @brief Upper bound vector of the QP */
    Eigen::VectorXd lbA; /*!< @brief Constraint lower bound vector of the QP */
    Eigen::VectorXd ubA; /*!< @brief Constraint upper bound vector of the QP */
    double regularisation_weight; /*!< @brief Regularisation weight */
    Eigen::VectorXd k_reg; /*!< @brief Proportional gain for the damping in the regularisation term */

    QPSolver qp_solver;
    QPSolver::qpProblem qp;
    int number_of_constraints; /*!< @brief Number of constraints of the QP problem*/
    int number_of_variables; /*!< @brief Number of optimization variables of the QP problem*/
    Eigen::Matrix<double, 6, Eigen::Dynamic> a; /*!< @brief Matrix to ease comprehension */
    Eigen::Matrix<double, 6, 1> b; /*!< @brief Matrix to ease comprehension */

    // Trajectory variables
    TrajectoryGenerator trajectory; /*!< @brief TrajectoryGenerator object */
    panda_traj::TrajProperties traj_properties; /*!< @brief Properties of the trajectory */
};
}  // namespace Controller
#endif  // CONTROLLER_HPP
